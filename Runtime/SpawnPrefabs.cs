﻿using UnityEngine;

namespace ActionCode.PrefabSpawner
{
    /// <summary>
    /// Spawns Prefabs on the Scene.
    /// </summary>
	public sealed class SpawnPrefabs : MonoBehaviour 
	{
        [Tooltip("If selected, it will spawn the prefab on Start.")]
        public bool spawnOnStart = true;
        [Tooltip("The prefab with this index will be spawned.")]
        public int spawnPrefab = 0;
        [SerializeField, Tooltip("Put here all the prefabs you want to be spawned.")]
        private GameObject[] prefabs = null;

        private void Start () 
		{
            if (spawnOnStart) Spawn();
        }

        private void OnValidate()
        {
            spawnPrefab = Mathf.Clamp(spawnPrefab, 0, prefabs.Length - 1);
        }

        /// <summary>
        /// Spawns the current prefab with position and rotation from this GameObject.
        /// </summary>
        /// <returns>A instantiated GameObject from the prefab.</returns>
        public GameObject Spawn()
        {
            return Spawn(transform.position, transform.rotation);
        }

        /// <summary>
        /// Spawns the prefab with the given position and rotation.
        /// </summary>
        /// <param name="position"></param>
        /// <param name="rotation"></param>
        /// <returns>A instantiated GameObject from the prefab.</returns>
        public GameObject Spawn(Vector3 position, Quaternion rotation)
        {
            GameObject prefab = GetCurrentPrefab();
            if (prefab == null) return null;
            GameObject instance = Instantiate(prefab, position, rotation);
            instance.name = prefab.name;
            return instance;
        }

        /// <summary>
        /// Spawns the current prefab with position and rotation and returns a component from the instance.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns>A instantiated component from the prefab.</returns>
        public T Spawn<T>() where T : Component
        {
            return Spawn<T>(transform.position, transform.rotation);
        }

        /// <summary>
        /// Spawns the prefab with the given position and rotation and returns a component from the instance.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="position"></param>
        /// <param name="rotation"></param>
        /// <returns>A instantiated component from the prefab.</returns>
        public T Spawn<T>(Vector3 position, Quaternion rotation) where T : Component
        {
            GameObject instance = Spawn(position, rotation);
            return instance == null ?
                default :
                instance.GetComponent<T>();
        }

        /// <summary>
        /// Sets the transform position, rotation and scale from the given GameObject instance.
        /// </summary>
        /// <param name="instance"></param>
        public void SetTransform(GameObject instance)
        {
            instance.transform.SetPositionAndRotation(transform.position, transform.rotation);
            instance.transform.localScale = transform.localScale;
        }

        /// <summary>
        /// Returns the current prefab based on the <see cref="spawnPrefab"/>.
        /// </summary>
        /// <returns>A non-instantiated GameObject prefab.</returns>
        public GameObject GetCurrentPrefab()
        {
            bool validPrefab = spawnPrefab > -1 && spawnPrefab < prefabs.Length;
            if (!validPrefab) return null;

            return prefabs[spawnPrefab];
        }
    }
}