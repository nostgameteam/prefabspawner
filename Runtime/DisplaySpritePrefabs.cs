﻿using UnityEngine;

namespace ActionCode.PrefabSpawner
{
    /// <summary>
    /// Displays the prefab Sprite using a SpriteRenderer.
    /// </summary>
    #if UNITY_EDITOR
    [ExecuteInEditMode]
    #endif
    [RequireComponent(typeof(SpawnPrefabs))]
	[RequireComponent(typeof(SpriteRenderer))]
	public sealed class DisplaySpritePrefabs : MonoBehaviour 
	{
        [Tooltip("Path to look for the SpriteRenderer in the prefab. Leave it empty if on the prefab root or use '/' to navigate through the hierarchy.")]
        public string path;
        [SerializeField, Tooltip("SpawnerPrefab component.")]
        private SpawnPrefabs spawner;
        [SerializeField, Tooltip("SpriteRenderer component.")]
        private SpriteRenderer spriteRenderer;

        private void Reset()
        {
            spawner = GetComponent<SpawnPrefabs>();
            spriteRenderer = GetComponent<SpriteRenderer>();
        }

        private void OnValidate()
        {
            ReplaceSprite();
        }

        #if UNITY_EDITOR
        private void Update()
        {
            if (!Application.isPlaying) ReplaceSprite();
        }
        #endif

        /// <summary>
        /// Replaces the sprite using the <see cref="SpawnPrefabs.GetCurrentPrefab"/> sprite.
        /// </summary>
        public void ReplaceSprite()
        {
            GameObject prefab = spawner.GetCurrentPrefab();
            SpriteRenderer prefabRenderer = prefab?.transform.Find(path)?.GetComponent<SpriteRenderer>();
            spriteRenderer.sprite = prefabRenderer ? prefabRenderer.sprite : null;
        }
    }
}