# Changelog
All notable changes to this package will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.0.0] - 2020-06-07
### Added
- Release initial version
- Add DisplaySpritePrefabs script
- Add SpawnPrefabs script
- Add CHANGELOG
- Add LICENCE
- Add README
- Add initial files
- Initial commit