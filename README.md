# Prefab Spawner

### What is this package for? ###

* This package contains Prefabs spawner scripts for unity game projects.
* Unity minimum version: 2018.3

### What can I do with it? ###
* You can use ...
	* **SpawnPrefabs**: Spawns Prefabs on the Scene.
	* **DisplaySpritePrefabs**: Displays the prefab Sprite using a SpriteRenderer.

## Installation

### Using the Package Registry Server

Follow the instructions inside [here](https://cutt.ly/ukvj1c8) and the package **ActionCode-Prefab Spawner** 
will be available for you to install using the **Package Manager** windows.

### Using the Git URL

You will need a **Git client** installed on your computer with the Path variable already set. 

Use the **Package Manager** "Add package from git URL..." feature or add manually this line inside `dependencies` attribute: 

```json
"com.actioncode.prefab-spawner":"https://bitbucket.org/nostgameteam/prefabspawner.git"

### Who do I talk to? ###

* Repo owner and admin: **Hyago Oliveira** (hyagogow@gmail.com)